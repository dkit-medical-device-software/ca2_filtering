# if using a Makefile

default: ca2_paper.pdf

ca2_paper.pdf: ca2_paper.tex
	latexmk -pdf $(patsubst %.pdf, %, $@)

ca2_graph.pdf: ca2_analysis.py
	python3 $^ $@

# this is very basic, you need to set up your dependencies as appropriate
