import numpy as np
import matplotlib
matplotlib.use("PDF")
from matplotlib import pyplot as plt
import sys

def main():
    output_filename = sys.argv[1]
    print("ca2 analysis")
    print("outputting to: %s" % output_filename)

    t = [ 0, 1, 2, 3]
    x = [ 1, 3, 2, 1]
    y = [ 8, 3, 1, 3]
    
    plt.figure()
    plt.plot(t,x)
    plt.plot(t,y)
    plt.savefig("%s" % output_filename)
    plt.close()
    
if __name__=='__main__':
    main()
